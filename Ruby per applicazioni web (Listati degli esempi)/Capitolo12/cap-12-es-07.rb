class HelloWorldServlet < WEBrick::HTTPServlet::AbstractServlet
  def do_GET(req,res)
    res.body = "<h1>Hello, world!</h1>"
  end
end

s = WEBrick::HTTPServer.new(:Port            => 10000,
                                                :DocumentRoot    => Dir.pwd)
trap("INT"){ s.shutdown }
s.mount "/hello", HelloWorldServlet
s.start
