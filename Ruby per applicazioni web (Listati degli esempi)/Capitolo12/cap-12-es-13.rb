class ERubyHandler < WEBrick::HTTPServlet::ERBHandler
  ERuby = "/usr/bin/eruby"

  def initialize(server, name)
    super
    @script_filename = name
  end

  def do_GET(req, res)
    IO.popen("#{ERuby} #{@script_filename}") do |io|
      res.body = io.read
    end
    res["content-type"] = "text/html"
  end
end
