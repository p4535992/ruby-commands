require 'webrick'

class DayTimeServer < WEBrick::GenericServer
  def initialize
    super :Port => 10000
  end
  def run sock
    sock.puts Time.now
  end
end

s = DayTimeServer.new
trap("INT") {s.shutdown}
s.start
