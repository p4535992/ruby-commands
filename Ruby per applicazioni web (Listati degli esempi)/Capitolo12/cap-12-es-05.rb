require 'webrick'

root = ARGV.shift 
port = 10000

s = WEBrick::HTTPServer.new(:Port                     => port,
                                                :DocumentRoot    => root)
trap("INT"){ s.shutdown }

Thread.new do
  sleep 2
  system "/usr/bin/firefox -new-tab localhost:#{port}"
end

s.start
