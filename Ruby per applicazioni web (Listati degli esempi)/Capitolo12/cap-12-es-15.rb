proc = Proc.new do |req,res|
  res.body = "<h2>Hello world dall'oggetto proc</h2>"
end

proc_mount = WEBrick::HTTPServlet::ProcHandler.new proc 

s = WEBrick::HTTPServer.new(:Port            => 10000,
                            :DocumentRoot    => Dir.pwd)
trap("INT"){ s.shutdown }
s.mount "/prova", proc_mount
s.start
