WEBrick::HTTPServlet::FileHandler.add_handler("rhtml", ERubyHandler)

s = WEBrick::HTTPServer.new(:Port=> 10000)
s.mount("/", WEBrick::HTTPServlet::FileHandler, Dir.pwd, {:FancyIndexing => true})
trap("INT"){ s.shutdown }
s.start 
