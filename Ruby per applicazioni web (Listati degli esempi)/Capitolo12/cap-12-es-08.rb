class HelloWorldServlet < WEBrick::HTTPServlet::AbstractServlet
  def do_GET(req,res)
    res.body = "<h1>Hello, world!</h1>"
    raise WEBrick::HTTPStatus::OK
  end
end
