s = WEBrick::HighPerformanceServer.create(:Port            => 10000,
                                                                       :DocumentRoot    => Dir.pwd)
trap("INT"){ s.shutdown }
s.start
