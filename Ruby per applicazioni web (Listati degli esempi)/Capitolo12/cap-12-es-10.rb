root = "/var/www/"
s = WEBrick::HTTPServer.new(:Port                     => port)

s.mount("/", WEBRick::HTTPServlet::FileHandler, root,
		{:FancyIndexing => true}) 
