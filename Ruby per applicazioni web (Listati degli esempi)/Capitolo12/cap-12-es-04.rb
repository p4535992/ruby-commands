require 'webrick'
 
s = WEBrick::HTTPServer.new(:Port            => 10000,
                            :DocumentRoot    => "/usr/share/doc/debian/FAQ")
trap("INT"){ s.shutdown }
s.start
