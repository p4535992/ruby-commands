root = "/var/www/"
s = WEBrick::HTTPServer.new(:Port => 10000)
trap("INT"){ s.shutdown }

s.mount("/cgi-bin", WEBrick::HTTPServlet::FileHandler, root + "cgi-bin/",
                {:FancyIndexing => true}) 

s.start
