begin
  require 'gruff'
rescue LoadError
  require 'rubygems'
  require_gem 'gruff'
end
