require 'rubygems'
SPEC = Gem::Specification.new do |s|
  s.name      = "WebForms"
  s.version = "1.0.0"
  s.author    = "Mario Rossi"
  s.email     = "mario@rossi.com"
  s.homepage = "http://www.rossi.com/WebForms"
  s.platform = Gem::Platform::RUBY
  s.summary = "A library to check web forms validation"
  candidates = Dir.glob("{bin,docs,lib,tests}/**/*")
  s.files     = candidates.delete_if do |item|
                  item.include?("CVS") || item.include?("rdoc")
                end
  s.require_path      = "lib"
  s.autorequire       = "webforms"
  s.test_file         = "tests/ts_webforms.rb"
  s.has_rdoc          = true
  s.extra_rdoc_files = ["README"]
  s.add_dependency("rake", ">= 0.7")
end
