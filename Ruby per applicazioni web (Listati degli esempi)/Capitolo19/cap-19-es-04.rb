if __FILE__ == $0
 require 'test/unit'
 class TC_BBCode < Test::Unit::TestCase
    def test_creation
      bb = BBCode.new( "012345")
      # Attenzione in ruby non si dovrebbero testare esplicitamente i tipi
      assert_kind_of String, bb  
      assert_equal 6, bb.size
      bb2= bb.gsub /\d/, ""
      assert_instance_of BBCode, bb2
      assert_equal 0, bb2.size
    end
 end
end
