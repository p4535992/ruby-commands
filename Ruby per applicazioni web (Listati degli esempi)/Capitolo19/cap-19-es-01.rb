# Restituisce la somma dei numeri da 1 a num
def sum_n(num)
  if num == 1
    1
  else
    num+ sum_n(num-1)
  end		
end
