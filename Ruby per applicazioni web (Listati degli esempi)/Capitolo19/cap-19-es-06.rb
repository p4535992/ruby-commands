def to_html
    txt= dup
    {'b'=>'strong','i'=>'em','u'=>'u','quote'=>'blockquote'}.each do |bb,html|
      txt.gsub! /\[#{bb}\]/, "<#{html}>"
      txt.gsub! /\[\/#{bb}\]/, "</#{html}>"
    end
   txt
  end
