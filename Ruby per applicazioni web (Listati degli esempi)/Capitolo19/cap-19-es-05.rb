  def to_html
    txt= dup
    txt.gsub! /\[b\]/, "<strong>"	
    txt.gsub! /\[\/b\]/, "</strong>"
    txt.gsub! /\[i\]/, "<em>"	
    txt.gsub! /\[\/i\]/, "</em>"
    txt.gsub! /\[u\]/, "<u>"
    txt.gsub! /\[\/u\]/, "</u>"
    txt
  end



  def test_bold
      bb=BBCode.new("[b]bold[/b]")
      assert_equal "<strong>bold</strong>", bb.to_html
  end
  def test_underline
      bb=BBCode.new("[u]under[/u]")
      assert_equal "<u>under</u>", bb.to_html
  end
  def test_italic
      bb=BBCode.new("[i]italic[/i]")
      assert_equal "<em>italic</em>", bb.to_html
  end
  def test_combined
      bb=BBCode.new("[u][b][i]all[/i][/b][/u]")
      assert_equal "<u><strong><em>all</em></strong></u>", bb.to_html
  end



