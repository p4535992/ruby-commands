class BBCode < String

  def to_html
    txt= dup
    {'b'=>'strong','i'=>'em','u'=>'u','quote'=>'blockquote'}.each do |bb,html|
      txt.gsub! /\[#{bb}\]/i, "<#{html}>"
      txt.gsub! /\[\/#{bb}\]/i, "</#{html}>"
    end
    ['size','color'].each do |bb|
      txt.gsub! /\[#{bb}=(.*?)\]/, "<font #{bb}=\\1>"
      txt.gsub! /\[\/#{bb}\]/, "</font>"
    end
    
    txt.gsub!(/\[URL\](.*?)\[\/URL\]/i, "<a href=\"\\1\">\\1</a>")
    txt.gsub!(/\[URL=(.*?)\](.*?)\[\/URL\]/i, "<a href=\"\\1\">\\2</a>")
    
    txt.gsub!(/\[IMG=(.*?)\]/i, "<img src=\"\\1\" />")
    
    txt.gsub!(/\[EMAIL\](.*?)\[\/EMAIL\]/i, "<a href=\"mailto:\\1\">\\1</a>")
    
    txt
  end
end

if __FILE__ == $0
 require 'stringio'
 require 'test/unit'
  class TC_BBCode < Test::Unit::TestCase
    def assert_html(html, bb)
      assert_equal html, BBCode.new(bb).to_html
    end
    def test_creation
      bb = BBCode.new( "012345")
      assert_kind_of String, bb 
      assert_equal 6, bb.size
      bb2= bb.gsub /\d/, ""
      assert_instance_of BBCode, bb2
      assert_equal 0, bb2.size
    end
    def test_bold
      assert_html "<strong>bold</strong>", "[b]bold[/b]"
    end
    def test_underline
      assert_html "<u>under</u>", "[u]under[/u]"
    end
    def test_italic
      assert_html "<em>italic</em>", "[i]italic[/i]"
    end
    def test_combined
      assert_html "<u><strong><em>all</em></strong></u>", 
                        "[u][b][i]all[/i][/b][/u]"
    end
    
    def test_combined_case
      assert_html "<strong><em><u>all upcase</u></em></strong>",
                        "[B][I][U]all upcase[/U][/I][/B]"
    end
                      
    def test_font
      assert_html '<font color="blue">blu</font>',
                        '[color="blue"]blu[/color]'
      assert_html '<font size=10>10</font>',
                        "[size=10]10[/size]"
    end
    def test_email
      assert_html '<a href="mailto:p@e.com">p@e.com</a>',
                        "[email]p@e.com[/email]"
    end
    def test_img
      assert_html '<img src="foo" />',
                        "[img=foo]"
    end
    def test_url
      assert_html '<a href="foo">foo</a>',
                        "[url]foo[/url]"
      assert_html '<a href="bar">foo</a>',
                        "[url=bar]foo[/url]"
          
    end
    def test_all
      text= %{[u]hello[/u] my mail is [email]foo@p.com[/email]
      [quote][url=http:://p.com]homepage[/url][/quote]
      [IMG=img.png] [color="black"][size=100][b]bye![/b][/size][/color]}
      assert_html %{<u>hello</u> my mail is <a href="mailto:foo@p.com">foo@p.com</a>
      <blockquote><a href="http:://p.com">homepage</a></blockquote>
      <img src="img.png" /> <font color=\"black\"><font size=100><strong>bye!</strong></font></font>},
                        text
    end
 end
end
