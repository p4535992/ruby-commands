class Prova
  attr_reader :num
  def initialize(num)
    @num = num
  end
  def incrementa
    @num += 1
  end
end

p = Prova.new(0)

thread1 = Thread.new do
  100000.times {p.incrementa}
end
thread2 = Thread.new do
  100000.times {p.incrementa}
end

[thread1,thread2].each {|t| t.join}

puts "num vale: #{p.num}"
