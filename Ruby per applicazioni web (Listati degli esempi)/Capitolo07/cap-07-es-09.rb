require 'thread'

def calcola(i)
  i+=1
  i**2
end

# una coda con tre posti
queue= SizedQueue.new(3) 

3.times do |i|
  Thread.new do
    while true
      risultato= calcola(i)
      queue << risultato
    end
  end
end

consumer=Thread.new do
  puts queue.pop while true
end

consumer.join
