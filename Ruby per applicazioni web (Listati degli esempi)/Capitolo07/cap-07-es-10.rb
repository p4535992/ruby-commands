thread = Thread.new do
  # ...
  raise ArgumentError, "parametro errato"
end

begin
  thread.join
rescue StandardError => error
  puts "Eccezione sollevata: #{error}, #{error.class}"
end

puts "Fine del programma"

