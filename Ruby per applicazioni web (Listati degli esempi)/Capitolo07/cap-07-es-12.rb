require 'thread'

class Prova
  attr_reader :num
  def initialize(num)
    @num = num
    @mutex = Mutex.new
  end
  def incrementa
    @mutex.synchronize do 
      @num += 1
    end
  end
end

# Il resto del codice rimane uguale
