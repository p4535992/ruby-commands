require 'drb'
require 'biblioteca.rb'

bib = Biblioteca.new
bib.libri << Libro.new("Tolkien", "Il signore degli anelli", 1249)
bib.libri << Libro.new("Thomas", "Programming Ruby", 560)
bib.libri << Libro.new("Hansson", "Sviluppare applicazioni web con Rails", 570)

DRb.start_service "druby://localhost:20000", bib
DRb.thread.join
