require 'soap/rpc/driver'

autore = ARGV[0]
puts "Ricerca in corso per l'autore #{autore}"

server = SOAP::RPC::Driver.new("http://localhost:20000",
                               "http://localhost/biblioteca")
server.add_method("cerca_libri", "autore")

puts server.cerca_libri(autore)
