class Libro
  attr_reader :autore, :titolo, :pagine
  def initialize(autore, titolo, pagine)
    @autore = autore
    @titolo = titolo
    @pagine = pagine
  end
end

libri = []

libri << Libro.new("Tolkien", "Il signore degli anelli", 1249)
libri << Libro.new("Thomas", "Programming Ruby", 560)
libri << Libro.new("Hansson", "Sviluppare applicazioni web con Rails", 570)

file = File.open "biblioteca.marshal", "w"
Marshal.dump(libri, file)
file.close
