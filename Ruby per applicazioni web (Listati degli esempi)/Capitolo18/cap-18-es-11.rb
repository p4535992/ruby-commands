require 'soap/wsdlDriver'
require 'cgi'

class RicercaGoogle
  def initialize
    @soap = SOAP::WSDLDriverFactory.new("GoogleSearch.wsdl").create_rpc_driver
    @key = "6HdddDVQFHIUTRHx/apJf****nUMNoW5"
  end
  def search(query)
    result = @soap.doGoogleSearch(@key, query, 0, 5, false, "", false, "", "", "")
    result.resultElements.each do |elem|
      yield elem.title, elem["URL"], CGI.unescapeHTML(elem.snippet)
    end
    return [result.estimatedTotalResultsCount, result.searchTime]
  end
end

if __FILE__ == $0
  google = RicercaGoogle.new
  num,time = google.search(ARGV[0]) do |titolo,url,snip|
    puts "#{titolo}:\t#{url}"
    puts "#{snip}"
  end
  print "\n\nTrovati #{num} risultati.\n"
  puts "La ricerca ha impiegato #{time} secondi."
end
