require 'soap/rpc/standaloneServer'
require 'biblioteca'

uri = "http://localhost/biblioteca"

class SoapBiblioteca < SOAP::RPC::StandaloneServer
  def on_init
    bib = Biblioteca.new
    bib.libri << Libro.new("Tolkien", "Il signore degli anelli", 1249)
    bib.libri << Libro.new("Thomas", "Programming Ruby", 560)
    bib.libri << Libro.new("Hansson", "Sviluppare applicazioni web con Rails", 570)
    add_method(bib, "cerca_libri", "autore")
  end
end

server = SoapBiblioteca.new("Biblioteca", uri, "0.0.0.0", 20000)
trap('INT') { server.shutdown }
server.start
