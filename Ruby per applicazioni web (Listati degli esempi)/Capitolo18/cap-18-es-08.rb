class Fattoriale
  def calcola(numero)
    @numero = numero
    return calcola_prima_parte * calcola_seconda_parte
  end
  def calcola_prima_parte
    sleep rand(5)
    return fatt(@numero-10)
  end
  def calcola_seconda_parte
    sleep rand(5)
    return fatt(@numero,@numero-10)
  end
  def fatt(numero, limite = 0)
    return numero if numero == limite+1
    return numero * fatt(numero-1, limite)
  end
end

fattoriale = Fattoriale.new

DRb.start_service "druby://localhost:20000", fattoriale
DRb.thread.join
