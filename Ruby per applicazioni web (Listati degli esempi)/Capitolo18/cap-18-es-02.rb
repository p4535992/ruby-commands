class Libro
  attr_reader :autore, :titolo, :pagine
  def initialize(autore, titolo, pagine)
    @autore = autore
    @titolo = titolo
    @pagine = pagine
  end
end

file = File.open "biblioteca.marshal"
libri = Marshal.load(file)
file.close

libri.each do |libro|
  puts "#{libro.autore}, \"#{libro.titolo}\": #{libro.pagine} pagine"
end
