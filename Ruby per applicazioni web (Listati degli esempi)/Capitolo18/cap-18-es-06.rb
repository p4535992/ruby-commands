class Fattoriale
  def calcola(numero, limite = 0)
    return numero if numero == limite+1
    return numero * calcola(numero-1, limite)
  end
end

fattoriale = Fattoriale.new

DRb.start_service "druby://localhost:20001", fattoriale
DRb.thread.join
