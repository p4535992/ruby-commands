require 'drb'

autore = ARGV[0]
puts "Ricerca in corso per l'autore #{autore}"

DRb.start_service
oggetto = DRbObject.new(nil, 'druby://localhost:20000')

oggetto.cerca_libri(autore) do |str|
  puts str
end
