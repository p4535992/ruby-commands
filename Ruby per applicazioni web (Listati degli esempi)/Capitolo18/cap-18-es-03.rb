class Libro
  attr_reader :autore, :titolo, :pagine
  def initialize(autore, titolo, pagine)
    @autore = autore
    @titolo = titolo
    @pagine = pagine
  end
  def to_str
     "#@autore, \"#@titolo\": #@pagine pagine"
  end
end

class Biblioteca
  attr_accessor :libri
  def initialize
    @libri = []
  end
  def cerca_libri(autore = "")
    @libri.each do |libro|
      yield libro.to_str if libro.autore == autore
    end
  end
end
