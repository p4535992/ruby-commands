require 'rss/2.0'

rss = RSS::Rss.new("2.0")
chan = RSS::Rss::Channel.new
chan.description = "Feed RSS test"
chan.link = "http://nostroblog.it/rss2"
rss.channel = chan

2.times do |i|
  item = RSS::Rss::Channel::Item.new
  item.title = "News ##{i}"
  item.link = "http://nostroblog.it/news_#{i}"
  item.description = "Qui si parla della news ##{i}"
  chan.items << item
end

puts rss.to_s
