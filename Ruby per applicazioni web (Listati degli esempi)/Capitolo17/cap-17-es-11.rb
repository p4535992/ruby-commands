def striphtml(line)
  line.gsub(/\n/, ' ').gsub(/<[^>]+>/, '') 
end 
