def crea_messaggi(from, to)
  messaggi = []
  open('http://weblog.rubyonrails.org/xml/rss20/feed.xml') do |http|
    response = http.read
    result = RSS::Parser.parse(response, false)
    puts "Titolo del feed: " + result.channel.title
    result.items.each_with_index do |item, i|
      item.title = striphtml(item.title)
      item.description = striphtml(item.description)
      messaggi << crea_messaggio(from, to, item) if i < 5
    end
  end
  
  messaggi
end
