require 'rubygems'
require_gem 'feedtools'

feed = FeedTools::Feed.open('http://www.planetrubyonrails.org/xml/atom')
puts "Titolo del feed: " + feed.title
feed.items.each_with_index do |item, i|
  puts "Titolo news ##{i+1}: #{item.title}" if i < 10
end
