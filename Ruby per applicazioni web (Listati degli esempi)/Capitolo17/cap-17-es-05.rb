require 'rss/2.0'
require 'open-uri'

open('http://weblog.rubyonrails.org/xml/rss20/feed.xml') do |http|
  response = http.read
  result = RSS::Parser.parse(response, false)
  puts "Titolo del feed: " + result.channel.title
  result.items.each_with_index do |item, i|
    puts "Titolo news ##{i+1}: #{item.title}" if i < 10
  end
end
