require 'rss/2.0'
require 'open-uri'
require 'net/smtp'

Net::SMTP.start("smtp.vostroserver.it", 25) do |smtp|
  from = "vostro@indirizzo.it"
  to = "indirizzo@destinatario.it"
  messaggi = crea_messaggi(from, to)

  messaggi.each_with_index do |msg, i| 
    smtp.send_message msg, from, to
    puts "messaggio #{i+1} mandato"
    sleep 3
  end

  puts "...tutti i messaggi sono stati inviati..."
end
