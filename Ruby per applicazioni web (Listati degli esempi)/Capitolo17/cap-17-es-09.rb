def crea_messaggio(from, to, item)
  messaggio = <<FINE_MESSAGGIO
From: #{from}
To: #{to}
Subject: #{item.title}

Autore: #{item.author}
Data: #{item.pubDate}
Link: #{item.link}

#{item.description}
FINE_MESSAGGIO
end
