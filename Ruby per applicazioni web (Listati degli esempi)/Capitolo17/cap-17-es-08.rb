require 'rubygems'
require_gem 'feedtools'

feed = FeedTools::Feed.new
feed.title = 'Feed Atom di prova'
feed.description = 'Il nostro feed Atom'
feed.link = 'http://nostroblog.it/atom1'

2.times do |i|
  item = FeedTools::FeedItem.new
  item.title = "News ##{i}"
  item.link = "http://nostroblog.it/news_#{i}/"
  item.summary = "Qui si parla della news ##{i}"
  feed.items << item
end

output_xml = feed.build_xml('atom', 1.0)
puts output_xml
