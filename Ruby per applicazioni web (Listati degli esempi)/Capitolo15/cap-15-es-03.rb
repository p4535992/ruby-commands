immagine = Magick::Image.new(200,200) do |imm|
    imm.background_color = "red"
end
immagine.write "profondo_rosso.png"
cerchio = Magick::Draw.new
cerchio.stroke("blue")
cerchio.stroke_width(6)
cerchio.ellipse(immagine.rows/2,immagine.columns/2, 80, 80, 0, 360)
cerchio.draw(immagine)

immagine.write "profondo_rosso-2.png"
