immagine = Magick::Image.new(200,200) do |imm|
    imm.background_color = "red"
end
immagine.write "profondo_rosso.png"
cerchio = Magick::Draw.new
cerchio.stroke("blue")
cerchio.stroke_width(6)
cerchio.ellipse(immagine.rows/2,immagine.columns/2, 80, 80, 0, 360)
cerchio.draw(immagine)

immagine.write "profondo_rosso-2.png"

text = Magick::Draw.new
text.font_family = 'helvetica'
text.pointsize = 26
text.gravity = Magick::CenterGravity

text.annotate(immagine, 0,0,2,2, "ruby power") do |txt|
  txt.fill = 'gray83'
end

text.annotate(immagine, 0,0,-1.5,-1.5, "ruby power") do |txt|
  txt.fill = 'gray40'
end

text.annotate(immagine, 0,0,0,0, "ruby power") do |txt|
  txt.fill = 'darkred'
end

immagine.write "profondo_rosso-3.png"
