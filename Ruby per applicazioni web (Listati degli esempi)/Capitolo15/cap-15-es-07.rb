emboss = immagine.emboss
emboss.write('funghi_emboss.jpg')

negate = immagine.negate
negate.write('funghi_negate.jpg')

oil_paint = immagine.oil_paint
oil_paint.write('funghi_oil_paint.jpg')

spread = immagine.spread
spread.write('funghi_spread.jpg')

blur = immagine.blur_image(0.0,2.5)
blur.write('funghi_blur.jpg')
