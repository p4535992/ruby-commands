require 'mrplot'
require 'mrplot/magick'
include MRPlot

titolo = Label.new "Titolo del grafico"
descrizione = Label.new "Descrizione del grafico"

rosso = Color.new(1,0,0)
verde = Color.new(0,1,0)
blu = Color.new(0,0,1)
nero = Color.new(0,0,0)
bianco = Color.new(1,1,1)

arr = []
1.upto(19) do |i|
  arr << {:x => i, :y => rand*9}
end
data = DataSet.new arr, "Dati di esempio

require 'mrplot/plots/xy'
plot = XYPlot.new titolo, descrizione

plot << data

plot.grid.style.solid = true
plot.grid.style.color = nero
plot.grid.division_style.solid = true

plot.axes.xlabel = "Asse x"
plot.axes.ylabel = "Asse y"

  gc = RMagickContext.new(Size.new(800,600))
plot.draw(gc, Rect.new(0,0,800,600), Border.new(30,30,20,20), Array.new, false, false)
gc.write("esempio.png")
