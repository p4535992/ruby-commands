g = Gruff::Line.new
g.title = "Fatturato e utile"

g.data "Fatturato", [10,25,20,13,18]
g.data "Utile", [4,9,7,8,11]
g.data "Spese", [5,18,15,3,13]
g.data "Debito", [15,13,17,10,7]

g.labels = {0 => "2001", 1 => "2002", 2 => "2003", 3 => "2004", 4 => "2005"}

g.write "fatturato4.png"
