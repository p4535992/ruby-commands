class UtentiFlickr < Flickr
  def users(lookup=nil)
    super(lookup)
  rescue RuntimeError => error
    if error.to_s =~ /User not found/
      puts "Utente non trovato! Specificare un altro nome"
      exit
    end
  end
end

flickr = UtentiFlickr.new KEY

user = flickr.users "marco.ceresa"
puts user.tags      
