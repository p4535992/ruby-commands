rrd.graph(["grafico.png",
            "--title", " RubyRRD Demo", 
            "--start", "#{start}",
            "--end", "#{Time.now.to_i}",
            "--imgformat", "PNG",
            "DEF:variabile1=#{rrd.rrdname}:variabile1:AVERAGE",
            "DEF:variabile2=#{rrd.rrdname}:variabile2:AVERAGE",
            "LINE1:variabile1#0000FF:Variabile numero 1",
            "AREA:variabile2#00FF00:Variabile numero 2"])
