require "RRDtool"
require 'time'

logfile = "/var/log/access.log"

result = Hash.new {|h,k| h[k]=1}
IO.foreach(logfile) do |line|
  if line =~ /.*\[(\d+)\/(\S+)\/(\d+):(\d+):(\d+).*\] "GET/
    time = Time.parse "#$1 #$2 #$3 #$4:#$5"
    result[time]+=1
  end
end

hash = result.sort{|a,b| a[0].to_i <=> b[0].to_i}

rrdname = "visitatori.rrd"
inizio  = hash[0][0].to_i
fine = hash[-1][0].to_i
quanto = 1 + (fine - inizio)/24

rrd = RRDtool.new(rrdname)

rrd.create(hash.size, inizio-1,       
           ["DS:visitatori:GAUGE:1800:U:U",
             "RRA:AVERAGE:0.5:1:#{quanto}"])

hash.each do |key,val|
  rrd.update("visitatori", ["#{key.to_i}:#{val}"])
end

rrd.graph(["visitatori.png",
            "--title", "Numero visitatori", 
            "--start", "#{inizio}",
            "--end", "#{fine}",
            "--zoom", "1.5",
            "--imgformat", "PNG",
            "DEF:visitatori=#{rrd.rrdname}:visitatori:AVERAGE",
            "AREA:visitatori#FFD700:Visitatori"])
