require "RRDtool"
rrdname = "database.rrd"
start   = Time.now.to_i

rrd = RRDtool.new(rrdname)
rrd.create(30, start-1,       
           ["DS:variabile1:COUNTER:60:U:U",
             "DS:variabile2:GAUGE:60:U:U",
             "RRA:AVERAGE:0.5:1:300"]) 

now = Time.now.to_i
valore1 = rand(1)
valore2 = rand(1)
rrd.update("variabile1", ["#{now}:#{valore1}"])
rrd.update("variabile2", ["#{now}:#{valore2}"])
