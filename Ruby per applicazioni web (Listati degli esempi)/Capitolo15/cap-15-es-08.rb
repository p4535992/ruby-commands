require 'RMagick'

FIXED_ROWS = 150 # pixel

$stdout.sync = true

ARGV.each do |file|

  print "Processing #{file} ... "  

  # definisce il nuovo nome
  name = file.sub(/\..*$/,".jpg")
  
  image = Magick::ImageList.new file
  ratio = FIXED_ROWS/image.rows.to_f

  # crea l'immagine scalata a 150 pixel di altezza
  new_image = image.scale ratio
  
  # aggiunge il bordo nero e salva il risultato
  bordo = Magick::Image.new(new_image.columns+4, new_image.rows+4) do |bordo|
    bordo.background_color = "black"
  end
  risultato = bordo.composite(new_image, Magick::CenterGravity,
                                                                   Magick::OverCompositeOp)
  risultato.write name
  
  # crea la thumbnail, 9% delle dimensioni originali
  thumb = image.thumbnail 0.09
  thumb.write "thumb-#{name}"
  
  puts "OK" 
  
end
