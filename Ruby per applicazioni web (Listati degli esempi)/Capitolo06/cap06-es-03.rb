require 'mysql'

begin
  dbh = Mysql.new("localhost", "usertest", "passwdtest", "test")
  dbh.close
rescue MysqlError => e
  print "Error code: ", e.errno, "\n"
  print "Error message: ", e.error, "\n"
end
