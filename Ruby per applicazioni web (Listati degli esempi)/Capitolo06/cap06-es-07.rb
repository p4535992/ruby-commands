require 'mysql'

begin
  dbh = Mysql.new("localhost", "usertest", "passwdtest", "test")
  result = dbh.query("SELECT title, director, year FROM movie")

  result.each_hash do |row|
    puts "#{row['title']} - #{row['director']} - #{row['year'].nil? ? "NULL" : row['year']}"
  end

  result.free
  dbh.close
rescue MysqlError => e
  print "Error code: ", e.errno, "\n"
  print "Error message: ", e.error, "\n"
end
