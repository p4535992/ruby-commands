require 'mysql'

begin
  dbh = Mysql.new("localhost", "usertest", "passwdtest", "test")
  dbh.query("DROP TABLE IF EXISTS movie")

  dbh.query("
  CREATE TABLE movie (
    title CHAR (60) NOT NULL,
    director CHAR (60) NOT NULL,
    year YEAR (4)
  );")

  puts "Tabella 'movie' creata."

  dbh.query("
  INSERT INTO movie (title, director, year) VALUES
    ('Blow Up', 'Michelangelo Antonioni', '1966'),
    ('Eraserhead', 'David Lynch', '1977'),
    ('Shining', 'Stanley Kubrick', '1980'),
    ('Blade Runner', 'Ridley Scott', '1982'),
    ('Once Upon A Time In America', 'Sergio Leone', '1984')")

  puts "#{dbh.affected_rows} record inseriti."
  dbh.close
rescue MysqlError => e
  print "Error code: ", e.errno, "\n"
  print "Error message: ", e.error, "\n"
end
