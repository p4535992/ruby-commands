require 'postgres'

begin
  dbh = PGconn.new("127.0.0.1", 5432, "", "", "test", "usertest", "passwdtest")
  resultset = dbh.query("SELECT * FROM movie;")
  resultset.each { |row| puts "#{row[0]} - #{row[1]} - #{row[2]}" }
  dbh.close
rescue PGError => e
  puts e.message
  dbh.close
end
