require 'rubygems'
require_gem 'activerecord'

ActiveRecord::Base.establish_connection({
  :adapter => "postgresql",
  :database => "test",
  :username => "usertest",
  :password => "passwdtest"
})

class Movie < ActiveRecord::Base
end

Movie.create :title => "Blade Runner", :director => "Ridley Scott"
Movie.create :title => "Arancia Meccanica", :director => "Stanley Kubrick"

Movie.find(:all).each { |movie| puts "#{movie.title} - #{movie.director}" }

