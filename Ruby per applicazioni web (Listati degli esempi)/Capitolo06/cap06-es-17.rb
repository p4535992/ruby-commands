require 'dbi'

begin
  dbh = DBI.connect("dbi:Mysql:test:localhost", "usertest", "passwdtest")
  result = dbh.do("SELECT VERSION ()")
  puts "Server version: #{row [0]}"
rescue DBI::DatabaseError => e
  puts "Error code: #{e.err}"
  puts "Error message: #{e.errstr}"
end
