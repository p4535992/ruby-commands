require 'mysql'

begin
  dbh = Mysql.new("localhost", "usertest", "passwdtest", "test")
  
  dbh.query("
  INSERT INTO movie (title, director, year) VALUES 
    ('Brazil', 'Terry Gilliam', NULL)")
  
  puts "#{dbh.affected_rows} record inseriti."
  dbh.close
rescue MysqlError => e
  print "Error code: ", e.errno, "\n"
  print "Error message: ", e.error, "\n"
end
