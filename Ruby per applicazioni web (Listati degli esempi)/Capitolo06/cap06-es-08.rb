require 'mysql'

begin
  dbh = Mysql.new("localhost", "usertest", "passwdtest", "test")
  title = dbh.escape_strings('C'era una volta in America')
  director = dbh.escape_strings('Sergio Leone')
  year = dbh.escape_strings('1984')

  dbh.query("INSERT INTO movie (title, director, year) VALUES 
                                       ('#{title}', '#{director}', '#{year}')")

  puts "#{dbh.affected_rows} record inseriti."
  dbh.close
rescue MysqlError => e
  print "Error code: ", e.errno, "\n"
  print "Error message: ", e.error, "\n"
end
