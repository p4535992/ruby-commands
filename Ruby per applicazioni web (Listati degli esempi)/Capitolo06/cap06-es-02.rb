require 'mysql'

begin
  dbh = Mysql.real_connect("localhost", "usertest", "passwdtest", "test")
  puts "Server version: #{dbh.server_info}\nClient version: #{dbh.client_info}\n"
  dbh.close
rescue MysqlError => e
  print "Error code: ", e.errno, "\n"
  print "Error message: ", e.error, "\n"
end
