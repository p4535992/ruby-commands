require 'postgres'

begin
  dbh = PGconn.new("127.0.0.1", 5432, "", "", "test", "usertest", "passwdtest")
  dbh.query("BEGIN;")
  dbh.query("CREATE TABLE movie (
  title VARCHAR (60) NOT NULL,
  director VARCHAR (60) NOT NULL,
  year CHAR (4));")
  dbh.query("INSERT INTO movie VALUES (#{PGconn.quote('Blade Runner')}, 
      #{PGconn.quote('Ridley Scott')}, #{PGconn.quote('1982')});")
  dbh.query("INSERT INTO movie VALUES (#{PGconn.quote('Arancia Meccanica')},
      #{PGconn.quote('Stanley Kubrick')}, #{PGconn.quote('1968')});")
  dbh.query("COMMIT;")
  puts "Tabella movie creata e popolata"
  dbh.close
rescue PGError => e
  puts e.message
  dbh.query("ROLLBACK;")
end
