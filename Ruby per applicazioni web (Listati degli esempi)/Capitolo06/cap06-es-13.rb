require 'postgres'

def movie_director(name)
  begin
    dbh = PGconn.new("127.0.0.1", 5432, "", "", "test", "usertest", "passwdtest")
    resultset = dbh.query("SELECT id FROM director WHERE name='#{name}';")

    if resultset.length == 0
      dbh.query("INSERT INTO director (name) VALUES ('#{PGconn.quote(name)}');")
      dbh.query("SELECT id FROM director WHERE name='#{name}';")
    else
      resultset
    end

  rescue PGError => e
    puts e.message
  end
end
