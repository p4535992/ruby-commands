require 'mysql'

def update_movie(original_title, title, director, year)
  begin
    dbh = Mysql.new("localhost", "usertest", "passwdtest", "test")
    dbh.autocommit(0)
 
   dbh.query("UPDATE movie SET title='#{title}' WHERE title='#{original_title}'")

    dbh.query("UPDATE movie SET director='#{director}' 
                                         WHERE title='#{original_title}'")

    dbh.query("UPDATE movie SET year='#{year}' WHERE title='#{original_title}'")

    dbh.commit
    dbh.close
    puts "Aggiornamento di #{original_title} eseguito con successo"
  rescue MysqlError
    dbh.rollback
    dbh.close
    puts "Aggiornamento di #{original_title} fallito"
  end
end

update_movie("Blade Runner", "Blade Runner", "Ridley Scott", "1982")
update_movie("C'era una volta in america", "C'era una volta in america", 
                                                                               "Sergio Leone", "1999")

