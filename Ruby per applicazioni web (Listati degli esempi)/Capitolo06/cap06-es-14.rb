def insert_movie (title, director, year)
  begin
    dbh = PGconn.new("127.0.0.1", 5432, "", "", "test", "usertest", "passwdtest")
    director = movie_director(director)

    dbh.query("INSERT INTO movie (title, director, year) 
                       VALUES (#{PGconn.quote(title)}, #{PGconn.quote(director)}, #{PGconn.quote(year)});")

    puts "Il film #{title} � stato inserito con successo"
    dbh.close
  rescue PGError => e
    puts e.message
  end
end
