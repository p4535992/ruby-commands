require 'rubygems'
require_gem 'activerecord'

ActiveRecord::Base.establish_connection({
  :adapter => "postgresql",
  :database => "test",
  :username => "postgres8"
})

class Movie < ActiveRecord::Base
end

movie = Movie.find_by_title("Blade Runner")
movie.title = "Alien"
movie.save

Movie.find(:all).each { |movie| puts "#{movie.title} - #{movie.director}" }
