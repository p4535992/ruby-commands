rrequire 'pstore'
require 'date'

@@blog = PStore.new("blog.pstore")

def add_entry(entry, datetime=nil)
  datetime = DateTime.now if datetime.nil?
  @@blog.transaction do
   @@blog[datetime.to_s] = entry
  end
end

def read_entries 
  @@blog.transaction do
    @@blog.roots.each do |datetime|
      puts datetime
      puts @@blog[datetime]
    end
  end
end
