require 'postgres'

begin
  dbh = PGconn.new("127.0.0.1", 5432, "", "", "test", "usertest", "passwdtest")
  puts "Server version: #{dbh.query("SHOW server_version").to_s}"
  dbh.close
rescue PGError => e
  puts e.message
end
