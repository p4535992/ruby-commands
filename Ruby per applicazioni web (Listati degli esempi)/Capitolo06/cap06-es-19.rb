require 'rubygems'
require_gem 'activerecord'

ActiveRecord::Base.establish_connection({
  :adapter => "postgresql",
  :database => "test",
  :username => "postgres8"
})

class Movie < ActiveRecord::Base
end

blade_runner = Movie.find_by_title("Blade Runner")
puts blade_runner.id
puts blade_runner.title
puts blade_runner.director
