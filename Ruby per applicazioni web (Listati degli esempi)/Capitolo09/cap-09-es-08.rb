require 'cgi'
require 'cgi/session'

class Pagina  
  
  attr_reader :pagina
  def initialize(nome_pagina, cgi)
    @pagina = nome_pagina
    @cgi = cgi
    ERuby.cgi = cgi
    session = CGI::Session.new(@cgi)
    if session["visitate"]
      @pagine = session["visitate"].split("|")
      @pagine << @pagina
    else
      @pagine = [@pagina]
    end
    @pagine.sort!.uniq!
    session["visitate"] = @pagine.join("|")
    session.close
  end

  def header
    @cgi.h2 {"Esempio di sito dinamico in ruby"} +
    @cgi.h4 {"Pagina corrente: #@pagina"} +
    @cgi.h4 {"Pagine visitate: #{@pagine.size}"}
  end
    
  def menu
    @cgi.a("prova2.rhtml") do
      "Pagina 2"
    end
  end

  def footer
    "<b> Powered by Ruby </b>"
  end

end
