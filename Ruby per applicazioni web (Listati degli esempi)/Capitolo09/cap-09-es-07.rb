def header
    @cgi.h2 {"Esempio di sito dinamico in ruby"} +
    @cgi.h4 {"Pagina corrente: #@pagina"} +
    @cgi.h4 {"Pagine visitate: #{@@visitate.size}"}
  end
    
  def menu
    @cgi.a("prova2.rhtml") do
      "Pagina 2"
    end
  end

  def footer
    "<b> Powered by Ruby </b>"
  end
