require 'mysql'

class Videoteca 
  def initialize
    @dbh = Mysql.new("localhost", "usertest", "passwdtest", "test")
  rescue MysqlError => e
    $stderr.print "Initialize error: ", e, "\n"
  end
  def cerca(stringa)
    result = @dbh.query("SELECT title, director, year FROM movie")
    result.each_hash do |row|
      a = [row["title"], row["director"], row["year"]]
      yield a if a.join =~ /#{stringa}/i
    end
  ensure
    result.free
    @dbh.close
  rescue MysqlError => e
    $stderr.print "Query error: ", e, "\n" 
  end
end
