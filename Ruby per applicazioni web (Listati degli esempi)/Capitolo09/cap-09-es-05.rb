class Pagina
  
  @@visitate = []
  
  attr_readers :pagina
  
  def initialize(nome_pagina, cgi)
    @pagina = nome_pagina
    @cgi = cgi
    unless @@visitate.include? nome_pagina
      @@visitate << nome_pagina
    end
  end

  def header
    # Generazione dell'header
  end

  def menu
    # Generazione del menu
  end

  def footer
    # Generazione del footer
  end

end
