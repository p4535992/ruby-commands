task :default => ["mail"]
task :all => :default

rule ".o" => ".c" do |f|
  sh "gcc -Wall -c -o #{f.name} #{f.source}"
end

file "mail" => ["form.o", "cgi.o", "mail.o"] do |f|
  sources = f.prerequisites.join(" ")
  sh "gcc #{sources} -o #{f.name} "
end
