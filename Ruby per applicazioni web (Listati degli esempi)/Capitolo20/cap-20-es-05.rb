require 'rubygems'
require 'tidy'

Tidy.path = "/usr/lib/libtidy.so"

File.open("index.html") do |html|
  Tidy.open do |tidy|
    tidy.clean(html)
    puts tidy.diagnostics
  end
end
