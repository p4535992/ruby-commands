require 'rake/clean'
require 'RMagick'

FIXED_ROWS = 150 # pixel

# definizione delle liste
sorgenti = FileList["*.png"]
jpeg = sorgenti.ext "jpg"
thumb = jpeg.collect {|name| "thumb-#{name}"}

# definizione delle directory
jpeg_dir  = "LowRes"
thumb_dir = "Thumbs"

# definizione del clean
CLEAN.include("**/*.jpg")

# regola di inferenza
rule ".jpg" => ".png" do |f|
  image = Magick::ImageList.new f.source
  ratio = FIXED_ROWS/image.rows.to_f
  new_image = image.scale ratio
  
  # crea l'immagine scalata a 150 pixel di altezza
  new_image = image.scale ratio
  
  # aggiunge il bordo nero e salva il risultato
  bordo = Magick::Image.new(new_image.columns+4, new_image.rows+4) do |bordo|
    bordo.background_color = "black"
  end
  risultato = bordo.composite(new_image, Magick::CenterGravity, 
                              Magick::OverCompositeOp)
  risultato.write f.name
end

# task per le directory
task :dir => [jpeg_dir, thumb_dir]

directory jpeg_dir
directory thumb_dir

# task per le thumbnail
task :thumb do 
  thumb.each_index do |i|
    name, source = thumb[i], sorgenti[i]
    image = Magick::ImageList.new source
    risultato = image.thumbnail 0.09
    risultato.write name
  end
end

# task principale
task :default => [jpeg, :thumb, :dir].flatten do
  jpeg.each do |file|
    mv file, jpeg_dir
  end
  thumb.each do |file|
    mv file, thumb_dir
  end
end
