def compila(name,sources)
  sh "gcc -Wall -c -o #{name} #{sources}"
end

file "form.o" => "form.c" do |f|
  compila f.name, f.prerequisites.join(" ")
end

file "cgi.o" => "cgi.c" do |f|
  compila f.name, f.prerequisites.join(" ")
end

file "mail.o" => "mail.c" do |f|
  compila f.name, f.prerequisites.join(" ")
end

file "mail" => ["form.o", "cgi.o", "mail.o"] do |f|
  sources = f.prerequisites.join(" ")
  sh "gcc #{sources} -o #{f.name} "
end

task :default => ["mail"]
task :all => :default
