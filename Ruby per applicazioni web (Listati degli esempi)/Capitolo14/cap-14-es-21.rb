require 'net/pop'

Net::POP3.start('server.it', 110, "username", "password") do |pop|
  pop.mails.each do |mail|
    File.open("inbox", 'a') do |f|
      mail.pop do |chunk|
        f.write chunk, "\n"
      end
    end
  end
end
