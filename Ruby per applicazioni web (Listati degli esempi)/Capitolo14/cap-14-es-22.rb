require 'net/pop'

Net::POP3.start('server.it', 110, "username", "password") do |pop|
  pop.mails.each do |mail|
    headers = mail.header

    from = headers.scan(/From: (.*)$/).flatten[0].chomp
    subj = headers.scan(/Subject: (.*)$/).flatten[0].chomp
    date = headers.scan(/Date: (.*)$/).flatten[0].chomp

    puts sprintf("[%-30s] [%-40s] [%-20s]",from,subj,date)
  end
end
