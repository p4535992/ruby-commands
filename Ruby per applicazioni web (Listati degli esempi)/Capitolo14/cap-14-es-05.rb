require 'uri'

uri = URI.parse("http://www.google.it/search?hl=it&q=ruby")
        #=> #<URI::HTTP:0xfdbd996c0 URL:http://www.google.it/search?hl=it&q=ruby>
uri.class        #=> URI::HTTP
uri.scheme    #=> "http"
uri.host         #=> "www.google.it"
uri.path         #=> "/search"
uri.query       #=> "hl=it&q=ruby" 

