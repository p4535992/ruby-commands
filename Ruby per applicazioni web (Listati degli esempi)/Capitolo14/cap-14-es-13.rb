require 'net/http'
loop do
  print "Inserisci il nome del file: "
  file = gets.chomp
  http = Net::HTTP.start("www.apogeonline.com")
  res = http.get("/"+file)
  case res
  when Net::HTTPNotFound
    puts "Non trovato"
  when Net::HTTPForbidden
    http.basic_auth 'account', 'password'
    res_auth = http.get("/"+file)
    puts res_auth.class    #=> In questo caso, Net::HTTPOk
  else
    puts "Altro codice"
  end
end
