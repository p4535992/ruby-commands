class Array
  def subset(re1,re2)
    arr = []
    each do |line|
      arr << line if (line =~ re1)..(line =~ re2)
    end
    arr
  end
end

["a","b","c","d","e","f"].subset(/c/,/e/) #=> ["c", "d", "e"]
