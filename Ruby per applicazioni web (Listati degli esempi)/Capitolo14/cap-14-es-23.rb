require 'net/telnet'
  
conn = Net::Telnet::new("Host" => "server.com",
                                       "Port" => 23,
                                       "Timeout" => 10)
conn.login("username", "password") { |str| print str }
conn.puts("pwd")
conn.cmd("uname -a") { |str| print str }
conn.close
