require 'net/http'
require 'uri'
url = "http://www.apogeonline.com/argomenti/"

class Redirection < StandardError
end

begin
  puts "Proviamo: #{url}"
  res = Net::HTTP.get_response URI.parse(url)
  case res
  when Net::HTTPRedirection
    raise Redirection
  else
    puts res.class
  end
rescue Redirection
  url = res["location"]
  retry
end
