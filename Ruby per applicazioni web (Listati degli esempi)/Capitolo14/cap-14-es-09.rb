class Array
  def subset(re1,re2)
    arr = []
    found = false
    each do |line|
      found = true if line =~ re1
      arr << line if found
      found = false if line =~ re2
    end
    arr
  end
end
