require 'net/http'

loop do
  print "Inserisci il nome del file: "
  file = gets.chomp
  http = Net::HTTP.start("www.apogeonline.com")
  res = http.get("/"+file)
  case res
  when Net::HTTPNotFound
    puts "Non trovato"
  when Net::HTTPForbidden
    puts "Vietato l'accesso"
  else
    puts "Altro codice"
  end
end
