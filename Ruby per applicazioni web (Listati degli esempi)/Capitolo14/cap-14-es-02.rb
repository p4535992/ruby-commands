require 'net/http'

http = Net::HTTP.start("www.apogeonline.com")
res = http.get("/")

lista = []

res.body.each_line do |line|
  if line =~ /href="([\w\/\.\-_]+)">?/
    lista << $1
  end
end

lista.each do |uri|
  res = http.head(uri)
  puts "#{uri}:\t#{res.message}"
end
