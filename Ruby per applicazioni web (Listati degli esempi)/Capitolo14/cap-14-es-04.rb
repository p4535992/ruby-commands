require 'net/http'
require 'time'

data = Time.parse("Jan 31")
pagine = ["http://ruby-it.org/index.html",
	          "http://www.ruby-doc.org/core/index.html"] 

pagine.each do |pagina|
  res = Net::HTTP.get_response URI.parse(pagina)
  if data < Time.parse(res.header["last-modified"])
    puts "La pagina #{pagina} e` stata modificata"
  end
end
