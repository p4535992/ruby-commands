require 'net/ftp'

Net::FTP.open("ftp.ruby-lang.org","anonymous","mail@ruby.org") do |ftp|
  ftp.passive = true
  ftp.chdir("/pub/ruby/stable/")
  file = ftp.nlst.grep(/^ruby/).last
  ftp.getbinaryfile(file)
end
