<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
                                                                        "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
<title>SMTP Web Client</title></head><body>
<% require 'cgi'
   require 'net/smtp'
   cgi = CGI.new
   if cgi.params.has_key? "rcpt"
     rcpt = cgi.params["rcpt"][0]
     subj = cgi.params["subj"][0]
     body = cgi.params["body"][0]
     from = "mio@indirizzo.com"
     mail = "From: #{from}\n"
     mail << "To: #{rcpt}\n"
     mail << "Subject: #{subj}\n"
     mail << "\n"
     mail << body

     begin 
       Net::SMTP.start("server.com", 25) do |smtp|
         smtp.send_message mail, from, rcpt 
       end
     rescue StandardError => e
       puts e 
     end %>
<h2>Mail inviata!</h2><br>
<%= mail.gsub("\n","<br>") %><br>
<% else %>
<form action="smtp.rhtml" method="POST">
Destinatario:<input type="TEXT" name="rcpt" size="50"><br>
Soggetto:<input type="TEXT" name="subj" size="50"><br>
Testo:<br><textarea name="body" rows="10" cols="50"></textarea><br>
<input type="SUBMIT" value="Invia!"></form>
<% end %>
</body> </html>
