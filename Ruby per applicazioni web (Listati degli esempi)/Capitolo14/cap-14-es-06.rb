require 'uri'

hosts = {"www.google.it" => "/search",
	        "www.excite.it" => "/search/web/results",
	        "it.altavista.com" => "/web/results"}
urls = []
hosts.each do |host,path|
  tmp = URI.build({:scheme => "http"})
  tmp.host = host
  tmp.path = path
  tmp.query = "q=ruby"
  urls << tmp
end

urls.each do |url|
  puts url.to_s
end
