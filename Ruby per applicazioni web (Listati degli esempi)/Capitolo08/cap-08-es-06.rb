require 'pstore'
require 'cgi'  

database = "movie.db"

cgi = CGI.new
movie_store = PStore.new database
movie_store.transaction do
  movie_store[:movie] = cgi.params
end
