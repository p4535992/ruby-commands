require 'cgi'

testo = '<Hitchcock> "rear window" & "psyco"'
             #=> <Hitchcock> "rear window" & "psyco"
testo_escaped = CGI::escapeHTML(testo)
           #=> "&lt;Hitchcock&gt; &quot;rear window&quot; &amp; &quot;psyco&quot;"
testo_unescaped = CGI::unescapeHTML(testo_escaped)
           #=> "<Hitchcock> \"rear window\" & \"psyco\""
