require 'cgi'
require 'cgi/session'
require 'time'

cgi = CGI.new "html4"
session = CGI::Session.new(cgi)

if session["ultimo_accesso"]
  cgi.out do
    last = Time.parse session["ultimo_accesso"]
    cgi.html do
      "Il tuo ultimo accesso e` avvenuto: #{last}" + cgi.br +
        "esattamente #{((Time.now-last)/3600).to_i} ore fa"
    end
  end
else
  cgi.out do 
    "Prima volta! Adesso il tuo accesso e` stato memorizzato"
  end
end

session["ultimo_accesso"] = Time.now.to_s   
session.close
