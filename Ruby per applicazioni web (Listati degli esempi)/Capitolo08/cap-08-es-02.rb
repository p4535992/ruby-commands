require 'cgi'

string = "Hello, world!"
cgi = CGI.new "html4"

cgi.out {
  cgi.html {
    cgi.head {cgi.title {"Pagina di prova - script CGI con Ruby"}} +
      "\n" + cgi.body { "\n" + 
        cgi.h2 {string} + "\n" +
        cgi.h4 { "Sono le ore #{Time.now.strftime('%H %M')}"}
    }
  }
} 
