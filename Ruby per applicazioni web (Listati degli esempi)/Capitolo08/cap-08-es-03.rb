require 'cgi'
cgi = CGI.new "html4"

cgi.a("http://www.ruby-lang.org") { "Il sito ufficiale" }
     #=> "<A HREF="http://www.ruby-lang.org">Il sito ufficiale</A>"
cgi.caption("left") { "Esempio di caption" }
     #=> "<CAPTION ALIGN="left">Esempio di caption</CAPTION>"
cgi.form("get","script.cgi", "text/plain") { "Esempio di form" }
     #=> "<FORM METHOD="get" ENCTYPE="text/plain" ACTION="script.cgi"> Esempio di form</FORM>"
cgi.submit("invia!")
     #=> "<INPUT TYPE="submit" VALUE="invia!">"
cgi.textarea("testo",40,5)
     #=> "<TEXTAREA NAME="testo" ROWS="5" COLS="40"></TEXTAREA>"
cgi.form("get","script.cgi","text/plain") {cgi.textarea("testo",40,5)+cgi.submit("invia!")}
     #=> <FORM METHOD="get" ENCTYPE="text/plain" ACTION="script.cgi">
     #  <TEXTAREA NAME="testo" ROWS="5" COLS="40">
     #</TEXTAREA>
     #<INPUT TYPE="submit" VALUE="invia!">
     #</FORM>
