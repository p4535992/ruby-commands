require 'cgi'

cgi = CGI.new
cgi['titolo']  #=> "psyco"
cgi['anno']  #=> "1960"

hash = cgi.params
p hash #=> {"anno"=>["1960"], "titolo"=>["psyco"]}
