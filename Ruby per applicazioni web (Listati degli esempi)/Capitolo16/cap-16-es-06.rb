require 'rexml/parsers/sax2parser'
require 'rexml/sax2listener'

class TitoloListener 
  include REXML::SAX2Listener
  def characters(text)
    puts "\"#{text}\""
  end
end

class LinguaListener
  include REXML::SAX2Listener
  def start_element(uri, localname, qname, attributes)
    print "#{attributes["lingua"]}:\t"
  end
end

titolo = TitoloListener.new
lingua = LinguaListener.new

file = File.new "biblioteca.xml"
doc = REXML::Parsers::SAX2Parser.new file

doc.listen(["titolo"],titolo)
doc.listen(["libro"],lingua)
doc.parse
