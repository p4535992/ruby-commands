require 'net/http'
require 'uri'
require 'cgi'
require 'rexml/document'

def search_artist(artist)
  artist = artist.map { |val| CGI.escape(val) }.join("+")
  resource = "http://musicbrainz.org/ws/1/artist?type=xml&name=#{ artist }"
  Net::HTTP.get(URI.parse(resource))
end

def parse_artist_list(document)
  document = REXML::Document.new(document)
  artist_list = []
  document.each_element('//artist') do |artist|
    id = artist.attributes['id']
    type = artist.attributes['type']
    name = artist.elements['name'].text
    artist_list.push({ "id" => id, "type" => type, "name" => name })
  end
  artist_list
end

parse_artist_list(search_artist("Rolling Stones")).each do |artist|
  puts "\n"
  puts "Nome: #{artist['name']}"
  puts "Tipo: #{artist['type']}"
  puts "MusicBrainz Artist ID: #{artist['id']}"
  puts "\n"
end
