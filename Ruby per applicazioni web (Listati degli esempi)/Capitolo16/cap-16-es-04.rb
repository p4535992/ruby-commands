require 'rexml/parsers/sax2parser'

file = File.new "biblioteca.xml"
doc = REXML::Parsers::SAX2Parser.new file

doc.listen(:characters, ["titolo"]) do |titolo|
  puts titolo
end

doc.parse
