file = File.new "biblioteca.xml"
doc = REXML::Parsers::SAX2Parser.new file

doc.listen(:characters, ["titolo"]) do |titolo|
  print "\"#{titolo}\": "
end

doc.listen(:characters, ["cognome"]) do |cognome|
  print "#{cognome}, "
end

doc.listen(:characters, ["pagine"]) do |pagine|
  puts "#{pagine} pagine."
end

doc.parse
