require 'postgres'

Libro = Struct.new :isbn,:titolo,:autore,:editore,:cover,:prezzo
Cliente = Struct.new :id,:nome,:cognome,:email,:indirizzo,:username,:password

module DBAux
  def insert(tab)
    str = "INSERT INTO #{tab} VALUES ("
    each do |val|
      str << "#{PGconn.quote(val)}, "
    end
    str.strip!.chomp!(",")
    str << ");"
  end
end

class Libro
  include DBAux
end
class Cliente
  include DBAux
end

class DBgestione
  def initialize
    @dbh = PGconn.new("127.0.0.1",5432,"","","negozio","usertest","passwdtest")
  end
  def inserisci(hsh, istanza, tab)
    istanza.each_pair do |sym,val|
      istanza[sym] = hsh[sym.to_s]
    end
    @dbh.query istanza.insert(tab)
    @dbh.query "COMMIT;"
    return "Record inseriti"
  end
  def close
    @dbh.close
  end
end
