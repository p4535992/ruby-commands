CREATE TABLE clienti (
  id SERIAL PRIMARY KEY,
  nome VARCHAR(80) NOT NULL,
  cognome VARCHAR(80) NOT NULL,
  email VARCHAR(80) NOT NULL,
  indirizzo VARCHAR(200) NOT NULL,
  username VARCHAR(20) NOT NULL,
  password VARCHAR(20) NOT NULL
);

CREATE TABLE libri (
  isbn VARCHAR(10) PRIMARY KEY,
  titolo VARCHAR(80) NOT NULL,
  autore VARCHAR(80) NOT NULL,
  editore VARCHAR(80) NOT NULL,
  cover BYTEA,
  prezzo DECIMAL(6,2) NOT NULL
);

CREATE TABLE magazzino (
  id_libro VARCHAR(10) REFERENCES libri (isbn),
  giacenza INTEGER NOT NULL
);

CREATE TABLE ordini (
  id SERIAL PRIMARY KEY,
  id_cliente INTEGER REFERENCES clienti (id),
  data TIMESTAMP NOT NULL
);

CREATE TABLE composizione_ordini (
  id_ordine INTEGER REFERENCES ordini (id),
  id_libro VARCHAR(10) REFERENCES libri (isbn),
  quantita SMALLINT NOT NULL
);

