class DBgestione
  def inserisci_ordine(cliente,ordini)
    @dbh.query "BEGIN;"
    @dbh.query "INSERT INTO ordini (id_cliente,data) " +
      "VALUES (#{cliente}, #{Time.now});"
    result = @dbh.query "SELECT id FROM ordini WHERE " + 
      "id_cliente = #{cliente} ORDER BY data DESC;"
    id = result[0][0]
    @dbh.query "COMMIT;"
    ordini.each do |isbn,numero|
      @dbh.query "BEGIN;"
      @dbh.query "INSERT INTO composizione_ordini VALUES " +
        "(#{id}, #{isbn}, #{numero});"
      @dbh.query "COMMIT;"
    end
  rescue PGError
    @dbh.query "ROLLBACK;"
  end
end
