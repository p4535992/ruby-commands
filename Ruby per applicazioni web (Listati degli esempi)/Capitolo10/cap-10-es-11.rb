def update_session(session,isbn,numero)
  if session.has_key? "#{isbn}"
    session["#{isbn}"] += numero
  else
    session["#{isbn}"] = numero
  end
end
def get_ordini(session)
  hsh = Hash.new
  session.each do |isbn,num|
    hsh[isbn] = num if isbn =~ /^\d+$/
  end
  hsh
end
