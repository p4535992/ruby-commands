class DBgestione
  def visualizza(query)
    result = @dbh.query("SELECT * FROM libri;")
    result.each do |riga|
      if query == ""
        yield riga
      else
        yield riga if riga[0..3].any? {|x| x =~ /#{query}/i}
      end
    end
  end
end
